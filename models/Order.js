// Mongoose Dependency
const mongoose = require('mongoose');


// Schema/Blueprint
const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		ref: 'User', // Reference to the User model
		required: true
	},
	products: [
		{
			productId: {
				type: String,
				ref: 'Product', // Reference to the Product model
				required: true
			},
			quantity: {
				type: Number,
				required: true
			},
	   },
	],
	totalAmount: {
		type: Number,
		required: true
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},

});


// Model
module.exports = mongoose.model('Order', orderSchema);