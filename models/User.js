// Mongoose Dependency
const mongoose = require('mongoose');

// Schema/Blueprint

const userSchema = new mongoose.Schema({
  fullname: {
    type: String,
    required: [true, 'Full name is required']
  },
  username: {
    type: String,
    required: [true, 'Username is required']
  },  
  email: {
    type: String,
    required: [true, 'Email is required']    
  },
  password: {
    type: String,
    required: [true, 'Password is required']
  },
  phoneNumber: {
    type: String,
    required: [true, 'Phone number is required']
  },
  isAdmin: {
    type: Boolean,
    default: false
  }
});


// Model
module.exports = mongoose.model('User', userSchema);