const Order = require('../models/Order');
const auth = require('../auth.js');
const User = require("../models/User");
const Product = require("../models/Product");


// CONTROLLER FUNCTION FOR NON-ADMIN USER CHECKOUT (CREATE ORDER)
module.exports.createOrder = async (req, res) => {
  try {
    // Check if the user is an admin
    if (req.user.isAdmin) {
      return res.send(false);
    }

    const { productId, quantity } = req.body;

    // Find the user by userId
    const user = await User.findById(req.user.id);

    // Find the product by name
    const product = await Product.findOne({ _id: productId });

    // Check if the product exists
    if (!product) {
      return res.status(404).json({ error: 'Product not found.' });
    }

    const order = new Order({
      userId: user.id,
      products: [
        {
          productId: product.id,
          quantity: quantity
        }
      ],
      totalAmount: product.price * quantity
    });

    // Save the order
    await order.save();

    res.status(201).json(true);
  } catch (error) {
    console.error(error);
    res.status(500).json(false);
  }
};

// CONTROLLER FUNCTION FOR RETRIEVING AUTHENTICATED USER'S ORDERS
module.exports.getUserOrders = async (req, res) => {
  try {
    // Retrieve the authenticated user's ID from the request object
    const userId = req.user.id;

    // Find the user's orders using the user ID
    const orders = await Order.find({ userId });

    // Return the orders as a response
    res.json(orders);
  } catch (error) {
    // Handle any errors that occur during the process
    console.error('Error retrieving user orders:', error);
    res.status(500).json({ message: 'Failed to retrieve user orders' });
  }
};



// CONTROLLER FUNCTION FOR RETRIEVING ALL ORDERS (ADMIN ONLY)
module.exports.getAllOrders = async (req, res) => {
  try {
  
  if (!req.user.isAdmin) {
    return res.send({ message: 'Access denied. Only admin users can retrieve all orders.' });
  }

    // Fetch all orders from the database
    const orders = await Order.find();

    res.json(orders);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};