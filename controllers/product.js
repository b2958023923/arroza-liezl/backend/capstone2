// Dependencies and Modules
const Product = require("../models/Product");
const auth = require('../auth.js');
const User = require("../models/User");





// CONTROLLER FUNCTION FOR CREATING A PRODUCT BY ADMIN
module.exports.addProduct = (req, res) => {
  let newProduct = new Product({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price
  });

  // Save the created object to the database
  return newProduct.save().then((product) => {
      // Product creation successful
      return res.send({ message: 'Product created successfully.', product });
    })
    .catch((error) => {
      // Product creation failed
      return res.send({ message: 'Error creating product.', error: error.message });
    });
};



// CONTROLLER FUNCTION FOR RETRIEVING ALL PRODUCTS
module.exports.getAllProducts = (req, res) => {
  return Product.find({}).then(result => {
    return res.send(result);
  })
  .catch(err => res.send(err))
};



// CONTROLLER FUNCTION FOR RETRIEVING ALL ACTIVE PRODUCTS
module.exports.getAllActiveProducts = (req, res) => {
  return Product.find({ isActive: true }).then(result => {

    return res.send(result)
  })
  .catch(err => res.send(err))
};



// CONTROLLER FUNCTION FOR RETRIEVING SINGLE PRODUCT BY NAME
module.exports.searchProductsByName = async (req, res) => {
  try {
    const { productName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const products = await Product.find({
      name: { $regex: productName, $options: 'i' }
    });

    res.json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};



// CONTROLLER FUNCTION FOR RETRIEVING SINGLE PRODUCT BY ID
module.exports.getProduct = (req, res) => {
  return Product.findById(req.params.productId).then(result => {  
 
    return res.send(result)
  })
  .catch(err => res.send(err))
};



// CONTROLLER FUNCTION FOR UPDATING A PRODUCT (ADMIN ONLY)
module.exports.updateProduct = (req, res) => {

  // Specify the fields/properties of the document to be updated 
  let updatedProduct = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price
  }

  return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) => { 

    // Product is not updated
    if(error) {
      return res.send(false);
    // Product updated successfully
    } else {
      return res.send(true);
    }   
  })
  .catch(err => res.send(err))
};



// CONTROLLER FUNCTION FOR ARCHIVING A PRODUCT (ADMIN ONLY)
module.exports.archiveProduct = (req, res) => {
  let updateActiveField = {
    isActive: false
  }
  return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
  .then((product, error) => {  

    // Product is not archived
    if(error) {
      return res.send(false)
    // Product archived successfully
    } else {      
      return res.send(true)
    }   
  })
  .catch(err => res.send(err))
};



// CONTROLLER FUNCTION FOR ACTIVATING AN ARCHIVED PRODUCT (ADMIN ONLY)
module.exports.activateProduct = (req, res) => {
  let activatedProduct = {
    isActive: true
  }
  return Product.findByIdAndUpdate(req.params.productId, activatedProduct).then((product, error) => { 

    // Product is not activated
    if(error) {
      return res.send(false)
    // Product activated successfully
    } else {      
      return res.send(true)
    }   
  })
  .catch(err => res.send(err))
};