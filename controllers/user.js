// Dependencies and Modules
const User = require("../models/User");

const bcrypt = require('bcrypt');
const auth = require('../auth.js');


// CONTROLLER FUNCTION TO CHECK IF EMAIL ALREADY EXISTS
module.exports.checkEmailExists = (reqBody) => {
  return User.find({email: reqBody.email}).then(result => {

    if(result.length > 0) {
      return true; 
    } else {
      return false;
    }
  })
};


// CONTROLLER FUNCTION TO REGISTER A NEW USER
module.exports.registerUser = (reqBody) => {

   let newUser = new User({
    fullname: reqBody.fullname,
    username: reqBody.username,
    email: reqBody.email,
    phoneNumber: reqBody.phoneNumber,
    password: bcrypt.hashSync(reqBody.password, 10)
  })

  // Saves the created object to our database
  return newUser.save().then((user, error) => {
    // User registratioon failed
    if(error) {
      return false;
    // User registration successful
    } else {
      return true;
    }
  }).catch(err => err)

};


// CONTROLLER FUNCTION FOR USER AUTHENTICATION
module.exports.loginUser = (req, res) => {
  return User.findOne({email: req.body.email}).then(result => {
    console.log(result);
    if(result == null) {
      return res.send(false)
    } else {

      const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

      if(isPasswordCorrect) {
       
        return res.send({access: auth.createAccessToken(result)})
      } else {
        // If password is incorrect.
        return res.send(false);
      }
    }
  }).catch(err => res.send(err));

};



// CONTROLLER FUNCTION FOR RETRIEVING USER DETAILS
module.exports.getProfile = (req, res) => {

	return User.findById(req.user.id).then(result => {

		result.password = "";

		return res.send(result);
	}).catch(err => res.send(err));
};



// CONTROLLER FUNCTION FOR SETTING USER AS ADMIN
module.exports.updateUserAsAdmin = async (req, res) => {
  const { userId } = req.body;

  try {
    
    if (!req.user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized. Only admin users can perform this action.' });
    }

    const user = await User.findByIdAndUpdate(userId, { isAdmin: true });

    if (!user) {
      return res.status(404).json({ message: 'User not found.' });
    }

    return res.status(200).json({ message: 'User updated as admin successfully.' });
  } catch (error) {
    console.error('Error updating user:', error);
    return res.status(500).json({ message: 'An error occurred while updating the user.' });
  }
};