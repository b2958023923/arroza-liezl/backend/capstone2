// Dependencies and Modules
const express = require('express');
const orderController = require('../controllers/order');
const auth = require('../auth');

// destructure the auth file:
// use verify and verifyAdmin as auth middlewares.
const { verify, verifyAdmin } = auth;


const router = express.Router();


// ROUTE FOR NON-ADMIN USER CHECKOUT (CREATE ORDER)
router.post('/checkout', verify, orderController.createOrder);


// ROUTE FOR RETRIEVING AUTHENTICATED USER'S ORDERS
router.get('/orderHistory', verify, orderController.getUserOrders);


// ROUTE FOR RETRIEVING ALL ORDERS (ADMIN ONLY)
router.get('/allOrders', verify, verifyAdmin, orderController.getAllOrders);


module.exports = router;