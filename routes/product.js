// Dependencies and Modules
const express = require('express');
const auth = require('../auth');
const productController = require('../controllers/product');

// destructure the auth file:
// use verify and verifyAdmin as auth middlewares.
const { verify, verifyAdmin } = auth;

const router = express.Router();




// ROUTE FOR CREATING A PRODUCT BY ADMIN
router.post('/', verify, verifyAdmin, productController.addProduct);


// ROUTE FOR RETRIEVING ALL PRODUCTS
router.get("/all", productController.getAllProducts);


// ROUTE FOR RETRIEVING ALL ACTIVE PRODUCTS
router.get("/", productController.getAllActiveProducts);


// ROUTE FOR RETRIEVING SINGLE PRODUCT BY NAME
router.post('/search', productController.searchProductsByName);

// ROUTE FOR RETRIEVING SINGLE PRODUCT BY PRICE RANGE
// router.post('/searchByPrice', productController.searchCoursesByPriceRange);

// ROUTE FOR RETRIEVING SINGLE PRODUCT BY ID
router.get("/:productId", productController.getProduct);


// ROUTE FOR UPDATING A PRODUCT (ADMIN ONLY)
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);


// ROUTE FOR ARCHIVING A PRODUCT (ADMIN ONLY)
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);


// ROUTE FOR ACTIVATING AN ARCHIVED PRODUCT (ADMIN ONLY)
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);




module.exports = router;