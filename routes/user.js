// Dependencies and Modules
const express = require('express');
const userController = require('../controllers/user');
const auth = require('../auth');

// destructure the auth file:
// use verify and verifyAdmin as auth middlewares.
const { verify, verifyAdmin } = auth;


const router = express.Router();


// ROUTE TO CHECK IF EMAIL ALREADY EXIST
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// ROUTE TO REGISTER A NEW USER 
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// ROUTE FOR USER AUTHENTICATION
router.post("/login", userController.loginUser);


// ROUTE FOR RETRIEVING USER DETAILS
router.get('/details', verify, userController.getProfile);


// ROUTE FOR SETTING USER AS ADMIN
router.put('/updateAsAdmin', verify, verifyAdmin, userController.updateUserAsAdmin);




module.exports = router;