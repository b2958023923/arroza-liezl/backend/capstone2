// Server Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/user');
const productRoutes = require('./routes/product');
const orderRoutes = require('./routes/order');



// Server Setup
const app = express();

// Environment Setup
const port = 8080;


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
// Allows all resources(frontend app) to access our backend application. Cross-origin resource sharing 
app.use(cors());


// Mongoose Connection
mongoose.connect("mongodb+srv://liezla:admin123@batch295.0fzdm7u.mongodb.net/Liezl-Capstone-API?retryWrites=true&w=majority", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);


let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection error'));
db.once('open', () => console.log('Connected to MongoDB Atlas.'));

// Home Route


// Backend Routes
// http://localhost:8080/users
app.use("/users", userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);




// Server Gateway Response

if(require.main === module) {
	app.listen(process.env.PORT || port, () => { console.log (`Server is now running in port ${process.env.PORT || port}.`)});
};


module.exports = (app, mongoose);
